@extends('admin.layout.master')
@section('content')
  <div class="row">
        <div class="col-md-12">

      <h2><strong>Post</strong></h2>
         @include('errors.message_error')
    </div>
  </div>
  <hr/>
  <div class="row">
    <div class="col-md-12">
    <a href="{{url('create_post')}}" class="btn btn-primary create">
                Create Post</a>
          <a href="{{url('test')}}" class="btn btn-primary create">
                test</a>
      <div class="panel panel-default">
        <div class="panel-heading">
                     List Posts
                </div><br>
               
                <div class="panel-body">        
                  <div class="table-responsive">
                  <form method="post"enctype='multipart/form-data'>
                  {{ csrf_field() }}
                  
                  <button formaction="{{url('multiple_delete_post')}}" type="submit" class="all_delete Delete" style="background:;color:red;margin-bottom:5px;">Delete all selected</button>
               
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                   
                          <thead>
                              <tr>
                                <th>description</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody class="item_list">
                            @foreach($tes as $ts)
                              <tr>
                                 <td>{{$ts->description}}</td>
                                 <td><a href="" class="btn btn-danger">edit</a></td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                    </form>  
                  </div>
              </div>
          </div>
    </div>
  </div>
<!-- <script src="{{url('js/multiple_checkbox.js')}}"></script> -->

@endsection
